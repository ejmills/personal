package DataBase;

public interface Recipe {
	public String getName();
	public String getCat();
	public String getType();
}
