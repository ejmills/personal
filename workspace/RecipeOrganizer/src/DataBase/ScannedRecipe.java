package DataBase;

import java.io.Serializable;

public class ScannedRecipe implements Recipe, Serializable{
	private static final long serialVersionUID = -3006433781905814306L;
	private String name;
	private String cat;
	private final String TYPE = "scan";
	
	public String getName() {
		return name;
	}

	public String getCat() {
		return cat;
	}
	
	public String getType(){
		return TYPE;
	}
}
