package DataBase;

import java.io.Serializable;

public class ManualRecipe implements Recipe, Serializable{
	private static final long serialVersionUID = 7462541888345754163L;
	private String name;
	private String cat;
	private final String TYPE = "man";
	
	public String getName() {
		return name;
	}

	public String getCat() {
		return cat;
	}

	public String getType(){
		return TYPE;
	}
}
