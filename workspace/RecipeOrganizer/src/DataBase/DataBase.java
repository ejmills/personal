package DataBase;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

public class DataBase implements Serializable{
	private static final long serialVersionUID = -8774654549593864591L;
	private ArrayList<Recipe> recipes;
	
	public DataBase(){
		recipes = new ArrayList<Recipe>();
	}
	
	public void save(File path) throws IOException{
		FileOutputStream fos = new FileOutputStream(path);
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		Recipe[] recipe = new Recipe[recipes.size()];
		recipes.toArray(recipe);
		
		oos.writeObject(recipe);
		
		oos.close();
	}
	
	public void load(File path) throws IOException{
		FileInputStream fis = new FileInputStream(path);
		ObjectInputStream ois = new ObjectInputStream(fis);
		Recipe[] recipe;
		
		try {
			recipe = (Recipe[])ois.readObject();
			recipes.addAll(Arrays.asList(recipe));
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		ois.close();
	}
	
	public ArrayList<Recipe> search(String text){
		ArrayList<Recipe> hits = new ArrayList<Recipe>();
		
		for(Recipe temp: recipes){
			if(temp.getName().contains(text)){
				hits.add(temp);
			}
		}
		
		return hits;
	}
	
	public ArrayList<Recipe> getRecipes(){
		return recipes;
	}
}
