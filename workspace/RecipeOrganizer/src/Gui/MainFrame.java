package Gui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;

import Controller.Controller;
import DataBase.Recipe;

public class MainFrame extends JFrame{
	private static final long serialVersionUID = 4185989615854832714L;
	private Toolbar toolbar;
	private Opener opener;
	private FilterPane filters;
	private String search;
	private ArrayList<Recipe> recipes;
	private JScrollPane scrollFilters;
	private JFileChooser chooser;
	private Controller controller;
	
	public MainFrame(){
		toolbar = new Toolbar();
		opener = new Opener();
		filters = new FilterPane();
		scrollFilters = new JScrollPane(filters);
		chooser = new JFileChooser();
		controller = new Controller();
		
		setJMenuBar(createMenu());
		
		toolbar.setListener(new Listener() {
			public void textPassed(String text) {
				if(text.equals("scan")){
					
				}
				else if(text.equals("man")){
					new AddWindowMan();
				}
				else{
					
				}
			}
		});
		
		filters.setListener(new Listener() {
			public void textPassed(String text) {
				if(text.equals("filter")){				
					filter(filters.getAppliedFilters());
				}
			}
		});
		
		
		
		scrollFilters.setHorizontalScrollBar(null);
		
		add(toolbar, BorderLayout.NORTH);
		add(scrollFilters, BorderLayout.WEST);
		add(opener, BorderLayout.CENTER);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(1400, 1000);
		setVisible(true);
		setResizable(true);
		setLocationRelativeTo(null);
		setLayout(new BorderLayout());
	}
	
	public ArrayList<Recipe> filter(ArrayList<String> filters){
		ArrayList<Recipe> temp = new ArrayList<Recipe>();
		
		for(int x = 0; x < filters.size(); x++){
			for(Recipe recipe: recipes){
				if(search == null){
					if(recipe.getCat().equals(filters.get(x))){
						temp.add(recipe);
					}
				}
				else{
					if(recipe.getCat().equals(filters.get(x)) && recipe.getName().contains(search)){
						temp.add(recipe);
					}
				}
			}
		}
		return temp;
	}
	
	private JMenuBar createMenu(){
		JMenuBar menuBar = new JMenuBar();
		
		JMenu fileMenu = new JMenu("File");
		
		JMenuItem saveItem = new JMenuItem("Save");
		JMenuItem loadItem = new JMenuItem("Open");
		
		fileMenu.add(saveItem);
		fileMenu.add(loadItem);
		
		loadItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.CTRL_MASK));
		saveItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));
		
		loadItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(chooser.showOpenDialog(MainFrame.this) == JFileChooser.APPROVE_OPTION){
					controller.load(chooser.getSelectedFile());
					recipes = controller.getRecipes();
				}
			}
		});
		
		saveItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(chooser.showSaveDialog(MainFrame.this) == JFileChooser.APPROVE_OPTION){
					controller.save(chooser.getSelectedFile());
				}
			}
		});
		
		menuBar.setBorder(BorderFactory.createEtchedBorder());
		
		menuBar.add(fileMenu);
		
		return menuBar;
	}
}
