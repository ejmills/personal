package Gui;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JCheckBox;
import javax.swing.JPanel;

public class FilterPane extends JPanel{
	private static final long serialVersionUID = 1608674457280829747L;
	private ArrayList<JCheckBox> filters;
	private ArrayList<String> appliedFilters;
	private GridBagConstraints gc;
	private Listener listener;
	
	public FilterPane(){
		Dimension dim = getPreferredSize();
		filters = new ArrayList<JCheckBox>();
		appliedFilters = new ArrayList<String>();
		
		dim.width = 250;
		dim.height = 1000;
		setPreferredSize(dim);
		
		setLayout(new GridBagLayout());
		
		gc = new GridBagConstraints();
		gc.gridx = 0;
		gc.gridy = 0;
		gc.weighty = .01;
		gc.weightx = 1;
		gc.anchor = GridBagConstraints.WEST;
	}
	
	public void setFilters(ArrayList<String> recipes){//ArrayList<Recipe> recipes){
		for(String temp: recipes){
			filters.add(new JCheckBox(temp));
		}
		
		for(int x = 0; x < recipes.size(); x++){
			if(x == recipes.size() -1){
				gc.weighty = 1;
				gc.anchor = GridBagConstraints.NORTHWEST;
				
				FilterPane.this.add(filters.get(x), gc);
			}
			else{
				gc.anchor = GridBagConstraints.WEST;
				FilterPane.this.add(filters.get(x), gc);
			}
			gc.gridy++;
		}
		
		for(JCheckBox temp: filters){
			temp.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					for(JCheckBox now: filters){
						if(now.isSelected()){
							appliedFilters.add(now.getText());
							listener.textPassed("filter");
						}
					}
				}
			});
			
		}
	}
	
	public ArrayList<String> getAppliedFilters(){
		return appliedFilters;
	}
	
	public void setListener(Listener listener){
		this.listener = listener;
	}
}
