package Gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JTextField;

public class Toolbar extends JComponent{
	private static final long serialVersionUID = -1256450389008072898L;
	private JButton searchBtn;
	private JButton addScanBtn;
	private JButton addManBtn;
	private JTextField searchTxt;
	private Listener listener;
	
	public Toolbar(){
		searchBtn = new JButton("Search");
		addScanBtn = new JButton("Add Scanned Recipe");
		addManBtn = new JButton("Add Manual Entry Recipe");
		searchTxt = new JTextField(75);
		
		searchBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				listener.textPassed(searchTxt.getText());
			}
		});
		
		addScanBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				listener.textPassed("scan");
			}
		});
		
		addManBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				listener.textPassed("man");
			}
		});
		
		setLayout(new GridBagLayout());
		setBorder(BorderFactory.createEtchedBorder());
		
		GridBagConstraints gc = new GridBagConstraints();
		gc.gridx = 0;
		gc.gridy = 0;
		gc.weightx = .1;
		gc.weighty = 1;
		gc.anchor = GridBagConstraints.EAST;
		
		add(addManBtn, gc);
		
		gc.gridx ++;
		gc.weightx = 1;
		gc.anchor = GridBagConstraints.WEST;
		
		add(addScanBtn, gc);
		
		gc.gridx ++;
		gc.anchor = GridBagConstraints.EAST;
		
		add(searchTxt, gc);
		
		gc.gridx ++;
		gc.anchor = GridBagConstraints.WEST;
		gc.weightx = .1;
		
		add(searchBtn);
	}
	
	public void setListener(Listener listener){
		this.listener = listener;
	}
}