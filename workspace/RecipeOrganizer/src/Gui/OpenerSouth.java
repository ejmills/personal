package Gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComponent;

public class OpenerSouth extends JComponent{
	private static final long serialVersionUID = 1L;
	private JButton openBtn;
	private JButton editBtn;
	private JButton deleteBtn;
	private Listener listener;
	
	public OpenerSouth(){
		openBtn = new JButton("Open");
		editBtn = new JButton("Edit");
		deleteBtn = new JButton("Delete");
		
		openBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				listener.textPassed("open");		
			}
		});
		
		editBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				listener.textPassed("edit");
			}
		});
		
		deleteBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				listener.textPassed("delete");
			}
		});
		
		setLayout(new GridBagLayout());
		
		GridBagConstraints gc = new GridBagConstraints();
		
		gc.gridx = 0;
		gc.gridy = 0;
		
		add(openBtn, gc);
		
		gc.gridx ++;
		
		add(editBtn, gc);
		
		gc.gridx ++;
		
		add(deleteBtn, gc);
	}
	
	public void setListener(Listener listener){
		this.listener = listener;
	}
}