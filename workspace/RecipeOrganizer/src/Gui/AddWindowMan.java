package Gui;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class AddWindowMan extends JFrame{
	private static final long serialVersionUID = -28424838237157746L;
	private TextArea ingr;
	private TextArea dir;
	private TextArea notes;
	private JLabel nameLbl;
	private JLabel catLbl;
	private JLabel notesLbl;
	private JLabel dirLbl;
	private JLabel ingrLbl;
	private JTextField name;
	private JTextField cat;
	private JButton okBtn;
	private JButton cancleBtn;
	
	public AddWindowMan(){
		Dimension dim = new Dimension(1000, 250);
		ingr = new TextArea();
		dir = new TextArea();
		notes = new TextArea();
		nameLbl = new JLabel("Name:");
		catLbl = new JLabel("Category;");
		notesLbl = new JLabel("Notes:");
		dirLbl = new JLabel("Directions:");
		ingrLbl = new JLabel("Ingredients:");
		name = new JTextField(50);
		cat = new JTextField(50);
		okBtn = new JButton("Ok");
		cancleBtn = new JButton("Cancle");
		
		ingr.setPreferredSize(dim);
		dir.setPreferredSize(dim);
		notes.setPreferredSize(dim);
		
		setLayout(new GridBagLayout());
		
		GridBagConstraints gc = new GridBagConstraints();
		
		gc.gridx = 0;
		gc.gridy = 0;
		gc.weightx = .01;
		gc.weighty = .01;
		gc.anchor = GridBagConstraints.WEST;
		add(nameLbl, gc);
		
		gc.gridy ++;
		add(name, gc);
		
		gc.gridy ++;
		add(catLbl, gc);
		
		gc.gridy ++;
		add(cat, gc);
		
		gc.gridy ++;
		add(ingrLbl, gc);
		
		gc.gridy ++;
		add(new JScrollPane(ingr), gc);
		
		gc.gridy ++;
		add(dirLbl, gc);
		
		gc.gridy ++;		
		add(new JScrollPane(dir), gc);
		
		gc.gridy ++;
		add(notesLbl, gc);
		
		gc.gridy ++;
		add(new JScrollPane(notes), gc);
		
		gc.gridy ++;
		gc.anchor = GridBagConstraints.EAST;
		//gc.weighty = 1;
		
		add(okBtn, gc);
		
		gc.gridx ++;
		gc.weightx = 1;
		gc.anchor = GridBagConstraints.WEST;
		add(cancleBtn, gc);
		
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setSize(1400, 1000);
		setVisible(true);
		setResizable(true);
		setLocationRelativeTo(null);
	}
}
