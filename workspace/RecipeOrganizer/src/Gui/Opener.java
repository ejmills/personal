package Gui;

import java.awt.BorderLayout;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JComponent;
import javax.swing.JList;

import DataBase.Recipe;

public class Opener extends JComponent{
	private static final long serialVersionUID = -630490147711608975L;
	private OpenerNorth north;
	private OpenerSouth south;
	private JList<String> recipes;
	private DefaultListModel<String> recipeList;
	private ArrayList<Recipe> visableRecipes;
	private Listener listener;
	
	public Opener(){
		north = new OpenerNorth();
		south = new OpenerSouth();
		recipes = new JList<String>();
		recipeList = new DefaultListModel<String>();
		
		recipes.setBorder(BorderFactory.createEtchedBorder());
		
		north.setListener(new Listener() {
			public void textPassed(String text) {
				listener.textPassed(text);		
			}
		});
		
		south.setListener(new Listener() {
			public void textPassed(String text) {
				if(text.equals("open")){				
					System.out.println(recipes.getSelectedIndex());
					new RecipeViewer(visableRecipes.get(recipes.getSelectedIndex()));
				}
			}
		});
		
		setLayout(new BorderLayout());
		
		add(north, BorderLayout.NORTH);
		add(south, BorderLayout.SOUTH);
		add(recipes, BorderLayout.CENTER);
	}
	
	public void search(String text){
		north.search(text);
	}
	
	public void load(ArrayList<Recipe> recipe){
		for(Recipe temp: recipe){
			recipeList.addElement(temp.getName());
		}
		
		recipes.setModel(recipeList);
		
		visableRecipes = recipe;
	}
	
	public void setListener(Listener listener){
		this.listener = listener;
	}
}
