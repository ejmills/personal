package Gui;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class TextArea extends JPanel{
	private static final long serialVersionUID = 7148955274757816072L;
	private JTextArea text;
	
	public TextArea(){
		text = new JTextArea();
		
		setLayout(new BorderLayout());
		
		add(new JScrollPane(text), BorderLayout.CENTER);
	}
	
	public String getText(){
		return text.getText();
	}
}
