package Gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;

public class OpenerNorth extends JComponent{
	private static final long serialVersionUID = -8412395479462585843L;
	private JLabel searchLabel;
	private JButton clearBtn;
	private Listener listener;
	
	public OpenerNorth(){
		searchLabel = new JLabel();
		clearBtn = new JButton("Clear Search");
		
		clearBtn.setVisible(false);
		clearBtn.setEnabled(false);
		
		clearBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				listener.textPassed("clear");
				clearBtn.setVisible(false);
				clearBtn.setEnabled(false);
				searchLabel.setVisible(false);
			}
		});
		
		setLayout(new GridBagLayout());
	
		GridBagConstraints gc = new GridBagConstraints();
		gc.gridx = 0;
		gc.gridy = 0;
		gc.weightx = 1;
		gc.weighty = 1;
		gc.anchor = GridBagConstraints.EAST;
		
		add(searchLabel, gc);
		
		gc.gridx ++;
		gc.anchor = GridBagConstraints.WEST;
		
		add(clearBtn, gc);
	}
	
	public void setListener(Listener listener){
		this.listener = listener;
	}
	
	public void search(String text){
		searchLabel.setText("Showing recipes for: " + text);
		searchLabel.setVisible(true);
		clearBtn.setVisible(true);
		clearBtn.setEnabled(true);
	}
}
