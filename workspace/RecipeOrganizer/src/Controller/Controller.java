package Controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import DataBase.DataBase;
import DataBase.Recipe;

public class Controller {
	private DataBase db;
	
	public Controller(){
		db = new DataBase();
	}
	
	public ArrayList<Recipe> search(String text){
		return db.search(text);
	}
	
	public void save(File path){
		try {
			db.save(path);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void load(File path){
		try {
			db.load(path);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public ArrayList<Recipe> getRecipes(){
		return db.getRecipes();
	}
}
